const bannersData = [
{
	id: "01",
	title: "Zuitt Coding Bootcamp",
	description: "Opportunities for everyone, everywhere.",
	button: "Enroll Now"
}, 
{
	id: "02",
	title: "Error 404 - Page not found",
	description: "The page you're looking for is not found.",
	button: "Back to home"
}
]

export default bannersData;