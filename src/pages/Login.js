import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
//import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

/*export default function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
      if(email !=="" && email != null && password !== "") {

          setIsActive(true);
      } else {
          setIsActive(false);
      }
      
  }, [email, password]);

  function logInUser (e) {
    e.preventDefault();

    setEmail("");
    setPassword("");

    alert ("You are now logged in.")
  }

  return (
      <Form onSubmit={e => logInUser(e)}>
        <Form.Group controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control
				type="email"
				value={email}
				onChange={(e) => setEmail(e.target.value)}
            />
            </Form.Group>
        <Form.Group controlId="password">
          	<Form.Label>Password</Form.Label>
	          	<Form.Control
	            type="password"
	            value={password}
	            onChange={(e) => setPassword(e.target.value)}
	          	/>
        </Form.Group>
        <br/>
        {isActive ?
        <Button variant="success" type="submit" id="submitBtn">
            Submit
        </Button>
        :
        <Button variant="danger" type="submit" id="submitBtn" disabled>
            Submit
        </Button>
    	}
      </Form>
  );

};*/

export default function Login(){

    const {user, setUser} = useContext(UserContext);

    // to store values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    //const navigate = useNavigate();

    function authenticate(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);

            retrieveUserDetails(data.access);
            
             Swal.fire({
                   title: "Login Successful",
                   icon: "success",
                   text: "Welcome to Zuitt!"
               });

            } else {
             Swal.fire({
                   title: "Authentication Failed",
                   icon: "error",
                   text: "Please, check your login details and try again."
               })
                
            }
            
        })

        /*localStorage.setItem('email', email);

        setUser({email: localStorage.getItem('email')});

        
        navigate('/')

        alert("Successfully login!")*/

        setEmail('')
        setPassword('')
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(

        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
            <br/>
            {   isActive ?
                <Button variant="success" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
};
