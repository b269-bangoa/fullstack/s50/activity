import Banners from '../components/Banners';
import Highlights from '../components/Highlights';
//import bannersData from '../data/bannersData';

export default function Home () {

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere.",
		destination: "/courses",
		label: "Enroll Now"
	}

	return (

		<>
		{/*<Banners bannerProps={bannersData[0]}/>*/}
		<Banners data={data}/>
		<Highlights/>
		</>

		)
}
