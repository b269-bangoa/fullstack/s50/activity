import Banners from '../components/Banners';
//import bannersData from '../data/bannersData';

/*export default function Error ({bannerProps}) {
	return (

		<>
		<Banners bannerProps={bannersData[1]}/>
		</>

		)
};*/

export default function Error () {

	const data = {
		title: "Error 404 - Page not found",
		content: "The page you're looking for is not found.",
		destination: "/",
		label: "Back to home"
	}

	return (
		<Banners data={data}/>
		)
};