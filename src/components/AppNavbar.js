import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Link} from 'react-router-dom';
import {useContext} from 'react';

import UserContext from '../UserContext';

export default function AppNavbar() {

  //const [user, setUser] = useState(localStorage.getItem('email'));

  const {user} = useContext(UserContext);

  return (
    <Navbar className="navbar" bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
          {
            (user.id !== null) ? 
          <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
          :
          <>
          <Nav.Link as={Link} to="/login">Login</Nav.Link>
          <Nav.Link as={Link} to="/register">Register</Nav.Link>
          </>
          }
          
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
