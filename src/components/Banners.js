import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

/*export default function Banners({bannerProps}) {

return (
    <Row>
    	<Col className="p-5">
            <h1>{bannerProps.title}</h1>
            <p>{bannerProps.description}</p>

            {
                (bannerProps.button === "Enroll Now") ?

                <Button variant="primary">{bannerProps.button}</Button>
                :
                <Button as={Link} to="/" variant="primary">{bannerProps.button}</Button>
            }
        </Col>
    </Row>
	)
};*/

export default function Banners({data}) {

    const {title, content, destination, label}= data;

return (
    <Row>
        <Col className="p-5">
            <h1>{title}</h1>
            <p>{content}</p>

            {<Button as={Link} to={destination} variant="primary">{label}</Button>}
        </Col>
    </Row>
    )
}